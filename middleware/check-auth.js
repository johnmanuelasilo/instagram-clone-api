const jwt = require("jsonwebtoken");

module.exports = (req, res, next) => {
    try{
        const token = req.headers.authorization.split(" ")[1];
        const id = jwt.verify(token,"mock-project-secret-value").id;
        req.userData = id;
        next();
    }catch(error){
        res.status(401).json({message: "Authorization failed!"});
    }


};