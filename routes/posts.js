const express = require("express");
const posts = require("../models").post;
const images = require("../models").image;
const checkAuth = require("../middleware/check-auth");
const multer = require("multer");

const router = express.Router();

const MIME_TYPE_MAP = {
    'image/png': 'png',
    'image/jpeg': 'jpg',
    'image/jpg': 'jpg'
};

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        const isValid = MIME_TYPE_MAP[file.mimetype];
        let error = new Error("Invalid mime type");
        if(isValid){
            error = null;
        }
        cb(error, "images");
    },
    filename: (req, file, cb) => {
        const name = file.originalname.toLowerCase().split(' ').join('-');
        const ext = MIME_TYPE_MAP[file.mimetype];
        cb(null, name+'-'+Date.now() +'.'+ext);
    }
});

router.post(
    "",
    checkAuth,
    multer({storage: storage}).array("image"),
    (req,res,next)=>{
    const post = req.body;
    console.log(post);
    const url = req.protocol + '://' + req.get("host");
    console.log(url);
    posts.create({visibility: req.body.visibility,title: req.body.title,content: req.body.content, postedby: req.userData}).then((result)=>{   
        
        for(i=0;i<req.files.length;i++){
            result.createImage({
                imagepath: url + "/images/" + req.files[i].filename
            });
        }
        
        res.status(201).send({
            message: "Post successfully published.",
            post: {
                id: result.id,
                createdAt: result.createdAt,
                updatedAt: result.updatedAt
            }
        });
    }).catch(err=>{
        res.status(500).send({message: "Error publishing post."});
    });
})

module.exports = router;