const express = require("express");
const users = require("../models").user;
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");

const router = express.Router();

router.post("/login", (req,res) => {
    const login = req.body;
    users.findAll().then((result)=>{
        if(!result[0].password){
            return res.status(401).json({
                message: "User doesn't exist."
            });
        }
        else{
            const token = jwt.sign(
                {id: result[0].id}, 
                "mock-project-secret-value",
                {expiresIn: '1h'}
            );

            console.log(result[0].dataValues.password);

            bcrypt.compare(login.password,result[0].dataValues.password).then(result=>{
                
                if(result){
                    res.status(200).json({
                        message: "Authorized",
                        access_token: token,
                        expiresIn: 1
                    });
                }
                else{
                    res.status(401).json({
                        message: "Unauthorized."
                    });
                }
            });
            
            
        }
    }).catch(err=>{
        res.status(401).json({
            message: "Unauthorized."
        });
    });
});

router.post("/signup", (req,res) => {
    const post = req.body;
    users.findAll({
        where:{
            email:post.email
        }
    }).then(user=>{
        if(!user[0]){
            bcrypt.hash(post.password, 10).then(hash=>{
                users.create({
                    email: post.email,
                    password: hash,
                    firstname: post.firstname,
                    lastname: post.lastname,
                    gender: post.gender,
                    birthdate: post.birthdate
                }).then((result)=>{
                    res.status(201).json({
                        message: 'User created successfully'
                    });
                });
            });
            
        }else{
            res.status(500).json({
                message: 'Email address is already in use.'
            });
        }
    }).catch(err=>{
        res.status(500).json({
            message: 'Error occured. Cannot create user'
        });
    });

});

module.exports = router;