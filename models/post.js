'use strict';
module.exports = (sequelize, DataTypes) => {
  const post = sequelize.define('post', {
    title: DataTypes.STRING,
    content: DataTypes.STRING,
    visibility: DataTypes.STRING,
    postedby: DataTypes.INTEGER
  }, {});
  post.associate = function(models) {
    // associations can be defined here
    post.belongsTo(models.user, {foreignKey:'postedby'});
    post.hasMany(models.image);
  };
  return post;
};