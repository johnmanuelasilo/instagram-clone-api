'use strict';
module.exports = (sequelize, DataTypes) => {
  const image = sequelize.define('image', {
    imagepath: DataTypes.STRING,
    postId: DataTypes.INTEGER
  }, {});
  image.associate = function(models) {
    // associations can be defined here
    image.belongsTo(models.post, {foreignKey:'postId'});
  };
  return image;
};